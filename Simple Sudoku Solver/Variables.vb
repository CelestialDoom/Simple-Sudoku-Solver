﻿Imports Windows.UI.Popups

Module Variables

    Public ABoutInfo As String = "This app will (hopefully) help you solve Sudoku puzzles." & vbCrLf & vbCrLf & "• Why?" & vbCrLf & "• I was looking through the Microsoft Store, and found MANY Sudoku solver apps, but none of them matched up to what I wanted out of an app." & vbCrLf & vbCrLf & "• What's it written in?" & vbCrLf & "• It's been coded using Visual Studio 2017 Community Edition." & vbCrLf & "The whole source code will be available on GitHub, at some point in time, for you to see." & vbCrLf & vbCrLf & "• Who do I contact if there's a problem?" & vbCrLf & vbCrLf & "celestial_doom@derpymail.org" & vbCrLf & vbCrLf & "----------" & vbCrLf & vbCrLf & "Made with ♥ in the UK"

    Public ArrayMulti(8, 8) As Integer

    Public ArraySolution(8, 8) As Integer

    Public ArrayGrid(100) As Integer

    Public ButtonClicked As Integer

    Public NumberCount As Integer = 0

    Public IsMenuOpen As Boolean = False

    Public dt As DispatcherTimer = New DispatcherTimer()

    Public DisplayHintCntr As Integer = 0

    Public PreviousValue As String

    Public ChosenButton As Integer

    Public Class GeneratedRandomBoard
        Public Property board As Integer()()
    End Class

    Public LibraryInfo As String = "Sudoku.Solver.Lite" & vbCrLf & vbCrLf & "Copyright (c) 2017 Zhiliang Xu" & vbCrLf & vbCrLf & "(I'm NOT sure what licence this uses)" & vbCrLf & vbCrLf & "(If anyone knows, can they contact me and I will update this text)"

    Public MainButtonArray As Button()

    Public LabelArray As TextBlock()

End Module
