﻿' The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

Imports System.Net.Http
Imports Windows.UI
Imports Windows.UI.Popups
''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class MainPage
    Inherits Page

    Public Async Function displayCheatMessage() As Task
        Dim messageDialog = New MessageDialog("Are you sure you'd like to cheat?", "Simple Sudoku Solver UWP")
        messageDialog.Commands.Add(New UICommand("Yes", Nothing))
        messageDialog.Commands.Add(New UICommand("No", Nothing))
        messageDialog.DefaultCommandIndex = 0
        messageDialog.CancelCommandIndex = 1
        Dim cmdResult = Await messageDialog.ShowAsync()
        If cmdResult.Label = "Yes" Then
            Dim solved As Boolean = SudokuSolver.SudokuSolver.Solve(ArrayMulti)
            Dim Lines(8) As String
            If solved Then
                For i As Integer = 0 To 9 - 1

                    For j As Integer = 0 To 9 - 1
                        Lines(i) = Lines(i) & ArrayMulti(i, j) & " "

                    Next
                Next
                ShowBoard()
            End If
        End If
    End Function

    Public Async Function displayMessageAsync(ByVal title As String, ByVal content As String, ByVal dialogType As String) As Task
        Dim messageDialog = New MessageDialog(content, title)
        If dialogType = "notification" Then
        Else
            messageDialog.Commands.Add(New UICommand("Yes", Nothing))
            messageDialog.Commands.Add(New UICommand("No", Nothing))
            messageDialog.DefaultCommandIndex = 0
        End If
        messageDialog.CancelCommandIndex = 1
        Dim cmdResult = Await messageDialog.ShowAsync()
        If cmdResult.Label = "Yes" Then
            Application.Current.Exit()
        End If
    End Function

    'This section concerns the buttons pressed.
    'It sets which button has been clicked
    'and then calls the SetNumbers subroutine

    Private Sub btn00_Click(sender As Object, e As RoutedEventArgs) Handles btn00.Click
        ButtonClicked = 0
        SetNumbers(True)
    End Sub

    Private Sub btn01_Click(sender As Object, e As RoutedEventArgs) Handles btn01.Click
        ButtonClicked = 1
        SetNumbers(True)
    End Sub

    Private Sub btn02_Click(sender As Object, e As RoutedEventArgs) Handles btn02.Click
        ButtonClicked = 2
        SetNumbers(True)
    End Sub

    Private Sub btn03_Click(sender As Object, e As RoutedEventArgs) Handles btn03.Click
        ButtonClicked = 3
        SetNumbers(True)
    End Sub

    Private Sub btn04_Click(sender As Object, e As RoutedEventArgs) Handles btn04.Click
        ButtonClicked = 4
        SetNumbers(True)
    End Sub

    Private Sub btn05_Click(sender As Object, e As RoutedEventArgs) Handles btn05.Click
        ButtonClicked = 5
        SetNumbers(True)
    End Sub

    Private Sub btn06_Click(sender As Object, e As RoutedEventArgs) Handles btn06.Click
        ButtonClicked = 6
        SetNumbers(True)
    End Sub

    Private Sub btn07_Click(sender As Object, e As RoutedEventArgs) Handles btn07.Click
        ButtonClicked = 7
        SetNumbers(True)
    End Sub

    Private Sub btn08_Click(sender As Object, e As RoutedEventArgs) Handles btn08.Click
        ButtonClicked = 8
        SetNumbers(True)
    End Sub

    Private Sub btn10_Click(sender As Object, e As RoutedEventArgs) Handles btn10.Click
        ButtonClicked = 9
        SetNumbers(True)
    End Sub

    Private Sub btn11_Click(sender As Object, e As RoutedEventArgs) Handles btn11.Click
        ButtonClicked = 10
        SetNumbers(True)
    End Sub

    Private Sub btn12_Click(sender As Object, e As RoutedEventArgs) Handles btn12.Click
        ButtonClicked = 11
        SetNumbers(True)
    End Sub

    Private Sub btn13_Click(sender As Object, e As RoutedEventArgs) Handles btn13.Click
        ButtonClicked = 12
        SetNumbers(True)
    End Sub

    Private Sub btn14_Click(sender As Object, e As RoutedEventArgs) Handles btn14.Click
        ButtonClicked = 13
        SetNumbers(True)
    End Sub

    Private Sub btn15_Click(sender As Object, e As RoutedEventArgs) Handles btn15.Click
        ButtonClicked = 14
        SetNumbers(True)
    End Sub

    Private Sub btn16_Click(sender As Object, e As RoutedEventArgs) Handles btn16.Click
        ButtonClicked = 15
        SetNumbers(True)
    End Sub

    Private Sub btn17_Click(sender As Object, e As RoutedEventArgs) Handles btn17.Click
        ButtonClicked = 16
        SetNumbers(True)
    End Sub

    Private Sub btn18_Click(sender As Object, e As RoutedEventArgs) Handles btn18.Click
        ButtonClicked = 17
        SetNumbers(True)
    End Sub

    Private Sub btn20_Click(sender As Object, e As RoutedEventArgs) Handles btn20.Click
        ButtonClicked = 18
        SetNumbers(True)
    End Sub

    Private Sub btn21_Click(sender As Object, e As RoutedEventArgs) Handles btn21.Click
        ButtonClicked = 19
        SetNumbers(True)
    End Sub

    Private Sub btn22_Click(sender As Object, e As RoutedEventArgs) Handles btn22.Click
        ButtonClicked = 20
        SetNumbers(True)
    End Sub

    Private Sub btn23_Click(sender As Object, e As RoutedEventArgs) Handles btn23.Click
        ButtonClicked = 21
        SetNumbers(True)
    End Sub

    Private Sub btn24_Click(sender As Object, e As RoutedEventArgs) Handles btn24.Click
        ButtonClicked = 22
        SetNumbers(True)
    End Sub

    Private Sub btn25_Click(sender As Object, e As RoutedEventArgs) Handles btn25.Click
        ButtonClicked = 23
        SetNumbers(True)
    End Sub

    Private Sub btn26_Click(sender As Object, e As RoutedEventArgs) Handles btn26.Click
        ButtonClicked = 24
        SetNumbers(True)
    End Sub

    Private Sub btn27_Click(sender As Object, e As RoutedEventArgs) Handles btn27.Click
        ButtonClicked = 25
        SetNumbers(True)
    End Sub

    Private Sub btn28_Click(sender As Object, e As RoutedEventArgs) Handles btn28.Click
        ButtonClicked = 26
        SetNumbers(True)
    End Sub

    Private Sub btn30_Click(sender As Object, e As RoutedEventArgs) Handles btn30.Click
        ButtonClicked = 27
        SetNumbers(True)
    End Sub

    Private Sub btn31_Click(sender As Object, e As RoutedEventArgs) Handles btn31.Click
        ButtonClicked = 28
        SetNumbers(True)
    End Sub

    Private Sub btn32_Click(sender As Object, e As RoutedEventArgs) Handles btn32.Click
        ButtonClicked = 29
        SetNumbers(True)
    End Sub

    Private Sub btn33_Click(sender As Object, e As RoutedEventArgs) Handles btn33.Click
        ButtonClicked = 30
        SetNumbers(True)
    End Sub

    Private Sub btn34_Click(sender As Object, e As RoutedEventArgs) Handles btn34.Click
        ButtonClicked = 31
        SetNumbers(True)
    End Sub

    Private Sub btn35_Click(sender As Object, e As RoutedEventArgs) Handles btn35.Click
        ButtonClicked = 32
        SetNumbers(True)
    End Sub

    Private Sub btn36_Click(sender As Object, e As RoutedEventArgs) Handles btn36.Click
        ButtonClicked = 33
        SetNumbers(True)
    End Sub

    Private Sub btn37_Click(sender As Object, e As RoutedEventArgs) Handles btn37.Click
        ButtonClicked = 34
        SetNumbers(True)
    End Sub

    Private Sub btn38_Click(sender As Object, e As RoutedEventArgs) Handles btn38.Click
        ButtonClicked = 35
        SetNumbers(True)
    End Sub

    Private Sub btn40_Click(sender As Object, e As RoutedEventArgs) Handles btn40.Click
        ButtonClicked = 36
        SetNumbers(True)
    End Sub

    Private Sub btn41_Click(sender As Object, e As RoutedEventArgs) Handles btn41.Click
        ButtonClicked = 37
        SetNumbers(True)
    End Sub

    Private Sub btn42_Click(sender As Object, e As RoutedEventArgs) Handles btn42.Click
        ButtonClicked = 38
        SetNumbers(True)
    End Sub

    Private Sub btn43_Click(sender As Object, e As RoutedEventArgs) Handles btn43.Click
        ButtonClicked = 39
        SetNumbers(True)
    End Sub

    Private Sub btn44_Click(sender As Object, e As RoutedEventArgs) Handles btn44.Click
        ButtonClicked = 40
        SetNumbers(True)
    End Sub

    Private Sub btn45_Click(sender As Object, e As RoutedEventArgs) Handles btn45.Click
        ButtonClicked = 41
        SetNumbers(True)
    End Sub

    Private Sub btn46_Click(sender As Object, e As RoutedEventArgs) Handles btn46.Click
        ButtonClicked = 42
        SetNumbers(True)
    End Sub

    Private Sub btn47_Click(sender As Object, e As RoutedEventArgs) Handles btn47.Click
        ButtonClicked = 43
        SetNumbers(True)
    End Sub

    Private Sub btn48_Click(sender As Object, e As RoutedEventArgs) Handles btn48.Click
        ButtonClicked = 44
        SetNumbers(True)
    End Sub

    Private Sub btn50_Click(sender As Object, e As RoutedEventArgs) Handles btn50.Click
        ButtonClicked = 45
        SetNumbers(True)
    End Sub

    Private Sub btn51_Click(sender As Object, e As RoutedEventArgs) Handles btn51.Click
        ButtonClicked = 46
        SetNumbers(True)
    End Sub

    Private Sub btn52_Click(sender As Object, e As RoutedEventArgs) Handles btn52.Click
        ButtonClicked = 47
        SetNumbers(True)
    End Sub

    Private Sub btn53_Click(sender As Object, e As RoutedEventArgs) Handles btn53.Click
        ButtonClicked = 48
        SetNumbers(True)
    End Sub

    Private Sub btn54_Click(sender As Object, e As RoutedEventArgs) Handles btn54.Click
        ButtonClicked = 49
        SetNumbers(True)
    End Sub

    Private Sub btn55_Click(sender As Object, e As RoutedEventArgs) Handles btn55.Click
        ButtonClicked = 50
        SetNumbers(True)
    End Sub

    Private Sub btn56_Click(sender As Object, e As RoutedEventArgs) Handles btn56.Click
        ButtonClicked = 51
        SetNumbers(True)
    End Sub

    Private Sub btn57_Click(sender As Object, e As RoutedEventArgs) Handles btn57.Click
        ButtonClicked = 52
        SetNumbers(True)
    End Sub

    Private Sub btn58_Click(sender As Object, e As RoutedEventArgs) Handles btn58.Click
        ButtonClicked = 53
        SetNumbers(True)
    End Sub

    Private Sub btn60_Click(sender As Object, e As RoutedEventArgs) Handles btn60.Click
        ButtonClicked = 54
        SetNumbers(True)
    End Sub

    Private Sub btn61_Click(sender As Object, e As RoutedEventArgs) Handles btn61.Click
        ButtonClicked = 55
        SetNumbers(True)
    End Sub

    Private Sub btn62_Click(sender As Object, e As RoutedEventArgs) Handles btn62.Click
        ButtonClicked = 56
        SetNumbers(True)
    End Sub

    Private Sub btn63_Click(sender As Object, e As RoutedEventArgs) Handles btn63.Click
        ButtonClicked = 57
        SetNumbers(True)
    End Sub

    Private Sub btn64_Click(sender As Object, e As RoutedEventArgs) Handles btn64.Click
        ButtonClicked = 58
        SetNumbers(True)
    End Sub

    Private Sub btn65_Click(sender As Object, e As RoutedEventArgs) Handles btn65.Click
        ButtonClicked = 59
        SetNumbers(True)
    End Sub

    Private Sub btn66_Click(sender As Object, e As RoutedEventArgs) Handles btn66.Click
        ButtonClicked = 60
        SetNumbers(True)
    End Sub

    Private Sub btn67_Click(sender As Object, e As RoutedEventArgs) Handles btn67.Click
        ButtonClicked = 61
        SetNumbers(True)
    End Sub

    Private Sub btn68_Click(sender As Object, e As RoutedEventArgs) Handles btn68.Click
        ButtonClicked = 62
        SetNumbers(True)
    End Sub

    Private Sub btn70_Click(sender As Object, e As RoutedEventArgs) Handles btn70.Click
        ButtonClicked = 63
        SetNumbers(True)
    End Sub

    Private Sub btn71_Click(sender As Object, e As RoutedEventArgs) Handles btn71.Click
        ButtonClicked = 64
        SetNumbers(True)
    End Sub

    Private Sub btn72_Click(sender As Object, e As RoutedEventArgs) Handles btn72.Click
        ButtonClicked = 65
        SetNumbers(True)
    End Sub

    Private Sub btn73_Click(sender As Object, e As RoutedEventArgs) Handles btn73.Click
        ButtonClicked = 66
        SetNumbers(True)
    End Sub

    Private Sub btn74_Click(sender As Object, e As RoutedEventArgs) Handles btn74.Click
        ButtonClicked = 67
        SetNumbers(True)
    End Sub

    Private Sub btn75_Click(sender As Object, e As RoutedEventArgs) Handles btn75.Click
        ButtonClicked = 68
        SetNumbers(True)
    End Sub

    Private Sub btn76_Click(sender As Object, e As RoutedEventArgs) Handles btn76.Click
        ButtonClicked = 69
        SetNumbers(True)
    End Sub

    Private Sub btn77_Click(sender As Object, e As RoutedEventArgs) Handles btn77.Click
        ButtonClicked = 70
        SetNumbers(True)
    End Sub

    Private Sub btn78_Click(sender As Object, e As RoutedEventArgs) Handles btn78.Click
        ButtonClicked = 71
        SetNumbers(True)
    End Sub

    Private Sub btn80_Click(sender As Object, e As RoutedEventArgs) Handles btn80.Click
        ButtonClicked = 72
        SetNumbers(True)
    End Sub

    Private Sub btn81_Click(sender As Object, e As RoutedEventArgs) Handles btn81.Click
        ButtonClicked = 73
        SetNumbers(True)
    End Sub

    Private Sub btn82_Click(sender As Object, e As RoutedEventArgs) Handles btn82.Click
        ButtonClicked = 74
        SetNumbers(True)
    End Sub

    Private Sub btn83_Click(sender As Object, e As RoutedEventArgs) Handles btn83.Click
        ButtonClicked = 75
        SetNumbers(True)
    End Sub

    Private Sub btn84_Click(sender As Object, e As RoutedEventArgs) Handles btn84.Click
        ButtonClicked = 76
        SetNumbers(True)
    End Sub

    Private Sub btn85_Click(sender As Object, e As RoutedEventArgs) Handles btn85.Click
        ButtonClicked = 77
        SetNumbers(True)
    End Sub

    Private Sub btn86_Click(sender As Object, e As RoutedEventArgs) Handles btn86.Click
        ButtonClicked = 78
        SetNumbers(True)
    End Sub

    Private Sub btn87_Click(sender As Object, e As RoutedEventArgs) Handles btn87.Click
        ButtonClicked = 79
        SetNumbers(True)
    End Sub

    Private Sub btn88_Click(sender As Object, e As RoutedEventArgs) Handles btn88.Click
        ButtonClicked = 80
        SetNumbers(True)
    End Sub

    'This section concerns the buttons pressed.
    'It sets which button has been clicked
    'and then calls the SetNumbers subroutine

    'This button shows the About information

    Private Sub btnAboutMenu_Click(sender As Object, e As RoutedEventArgs) Handles btnAboutMenu.Click
        CloseMenu()
        ShowInfo(0)
    End Sub

    'This button closes the information grid

    Private Sub btnClose_Click(sender As Object, e As RoutedEventArgs) Handles btnClose.Click
        _info.Visibility = Visibility.Collapsed
    End Sub

    'This button clears the chosen button of its value
    'by calling the EraseContent subroutine

    Private Sub btnErase_Click(sender As Object, e As RoutedEventArgs) Handles btnErase.Click
        SetNumbers(False)
        erasecontent(ButtonClicked)
    End Sub

    'This button shows the 3rd Party Libraries used

    Private Sub btnLibraryMenu_Click(sender As Object, e As RoutedEventArgs) Handles btnLibraryMenu.Click
        CloseMenu()
        ShowInfo(1)
    End Sub


    'This collection sets which number has been chosen
    'and sets the previously selected button with the
    'appropriate number.

    Private Sub btnHint_Click(sender As Object, e As RoutedEventArgs) Handles btnHint.Click
        ShowHint(ButtonClicked)
        SetNumbers(False)
    End Sub

    Private Sub btnNo1_Click(sender As Object, e As RoutedEventArgs) Handles btnNo1.Click
        changebuttonnumber(ButtonClicked, 1)
    End Sub

    Private Sub btnNo2_Click(sender As Object, e As RoutedEventArgs) Handles btnNo2.Click
        changebuttonnumber(ButtonClicked, 2)
    End Sub

    Private Sub btnNo3_Click(sender As Object, e As RoutedEventArgs) Handles btnNo3.Click
        changebuttonnumber(ButtonClicked, 3)
    End Sub

    Private Sub btnNo4_Click(sender As Object, e As RoutedEventArgs) Handles btnNo4.Click
        changebuttonnumber(ButtonClicked, 4)
    End Sub

    Private Sub btnNo5_Click(sender As Object, e As RoutedEventArgs) Handles btnNo5.Click
        changebuttonnumber(ButtonClicked, 5)
    End Sub

    Private Sub btnNo6_Click(sender As Object, e As RoutedEventArgs) Handles btnNo6.Click
        changebuttonnumber(ButtonClicked, 6)
    End Sub

    Private Sub btnNo7_Click(sender As Object, e As RoutedEventArgs) Handles btnNo7.Click
        changebuttonnumber(ButtonClicked, 7)
    End Sub

    Private Sub btnNo8_Click(sender As Object, e As RoutedEventArgs) Handles btnNo8.Click
        changebuttonnumber(ButtonClicked, 8)
    End Sub

    Private Sub btnNo9_Click(sender As Object, e As RoutedEventArgs) Handles btnNo9.Click
        changebuttonnumber(ButtonClicked, 9)
    End Sub

    'This clears and resets the board

    Private Sub btnReset_Click(sender As Object, e As RoutedEventArgs) Handles btnReset.Click
        Dim a As Integer = 0
        Do
            MainButtonArray(a).Content = "0"
            ArrayGrid(a) = 0
            a += 1
        Loop While a < 81

        SetNumbers(False)

        NumberCount = 0
        btnSolve.IsEnabled = False
        btnHint.IsEnabled = False

        For i As Integer = 0 To 9 - 1
            For j As Integer = 0 To 9 - 1
                ArrayMulti(i, j) = 0
                ArraySolution(i, j) = 0
            Next
        Next
    End Sub

    'This runs the solve routine using Sudoku.Solver.Lite
    'amd then shows the solution

    Private Async Sub btnSolve_Click(sender As Object, e As RoutedEventArgs) Handles btnSolve.Click
        Await displayCheatMessage()
    End Sub

    'This routine displays a brief hint
    'located at the chosen button

    Private Sub ShowHint(ByVal x As Integer)
        ChosenButton = x
        PreviousValue = MainButtonArray(x).Content
        MainButtonArray(x).Content = LabelArray(x).Text
        NumbersCheck(False)
        btnHint.IsEnabled = False
        dt.Start()
    End Sub

    'This routine changes the value of a selected button

    Private Sub changebuttonnumber(ByVal x As Integer, ByVal y As Integer)
        Dim a As Integer = 0
        Dim b As Integer = 0
        Dim c As Integer = 0
        MainButtonArray(x).Content = y.ToString
        ArrayGrid(x) = y
        Do
            ArrayMulti(a, b) = ArrayGrid(c)
            b += 1
            c += 1
            If b = 9 Then
                b = 0
                a += 1
            End If
        Loop While a < 9
    End Sub

    'This routine checks the amount of numbers
    'entered. The minimum amount required is 17
    'for the solve button to be visible

    Private Sub NumbersCheck(ByVal bool As Boolean)
        Select Case bool
            Case True
                NumberCount += 1
            Case False
                NumberCount -= 1
        End Select

        If NumberCount < 17 Then
            btnSolve.IsEnabled = False
            btnHint.IsEnabled = False
        Else
            btnSolve.IsEnabled = True
            btnHint.IsEnabled = True
        End If
    End Sub

    'This routine erases the value of a chosen button

    Private Sub erasecontent(ByVal x As Integer)
        Dim a As Integer = 0
        Dim b As Integer = 0
        Dim c As Integer = 0
        MainButtonArray(x).Content = "0"
        ArrayGrid(x) = 0
        Do
            ArrayMulti(a, b) = ArrayGrid(c)
            b += 1
            c += 1
            If b = 9 Then
                b = 0
                a += 1
            End If
        Loop While a < 9
    End Sub

    Public Sub dispatcherTimer_Tick(ByVal sender As Object, ByVal e As EventArgs)
        DisplayHintCntr += 1
        If DisplayHintCntr = 2 Then
            dt.Stop()
            DisplayHintCntr = 0
            MainButtonArray(ChosenButton).Content = PreviousValue
            btnHint.IsEnabled = True
        End If
    End Sub

    Public Shared Function GetAppVersion() As String
        Dim package As Package = Package.Current
        Dim packageId As PackageId = package.Id
        Dim version As PackageVersion = packageId.Version
        Return String.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision)
    End Function

    Private Sub MainPage_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Dim platformFamily = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily

        AddHandler dt.Tick, AddressOf dispatcherTimer_Tick
        dt.Interval = New TimeSpan(0, 0, 1)

        'This checks what sort of platform the app is running on
        If platformFamily = "Windows.Desktop" Then
            txtPhone.Visibility = Visibility.Collapsed
            txtNonPhone.Visibility = Visibility.Visible
        Else
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait
            txtPhone.Visibility = Visibility.Visible
            txtNonPhone.Visibility = Visibility.Collapsed
        End If

        For i As Integer = 0 To 9 - 1
            For j As Integer = 0 To 9 - 1
                ArrayMulti(i, j) = 0
                ArraySolution(i, j) = 0
            Next
        Next

        'Loads the main buttons of the game grid into array
        MainButtonArray = {btn00, btn01, btn02, btn03, btn04, btn05, btn06, btn07, btn08, btn10, btn11, btn12, btn13, btn14, btn15, btn16, btn17, btn18, btn20, btn21, btn22, btn23, btn24, btn25, btn26, btn27, btn28, btn30, btn31, btn32, btn33, btn34, btn35, btn36, btn37, btn38, btn40, btn41, btn42, btn43, btn44, btn45, btn46, btn47, btn48, btn50, btn51, btn52, btn53, btn54, btn55, btn56, btn57, btn58, btn60, btn61, btn62, btn63, btn64, btn65, btn66, btn67, btn68, btn70, btn71, btn72, btn73, btn74, btn75, btn76, btn77, btn78, btn80, btn81, btn82, btn83, btn84, btn85, btn86, btn87, btn88}

        'Loads the textblocks of the solution grid into array
        LabelArray = {txtGrid_0, txtGrid_1, txtGrid_2, txtGrid_3, txtGrid_4, txtGrid_5, txtGrid_6, txtGrid_7, txtGrid_8, txtGrid_10, txtGrid_11, txtGrid_12, txtGrid_13, txtGrid_14, txtGrid_15, txtGrid_16, txtGrid_17, txtGrid_18, txtGrid_20, txtGrid_21, txtGrid_22, txtGrid_23, txtGrid_24, txtGrid_25, txtGrid_26, txtGrid_27, txtGrid_28, txtGrid_30, txtGrid_31, txtGrid_32, txtGrid_33, txtGrid_34, txtGrid_35, txtGrid_36, txtGrid_37, txtGrid_38, txtGrid_40, txtGrid_41, txtGrid_42, txtGrid_43, txtGrid_44, txtGrid_45, txtGrid_46, txtGrid_47, txtGrid_48, txtGrid_50, txtGrid_51, txtGrid_52, txtGrid_53, txtGrid_54, txtGrid_55, txtGrid_56, txtGrid_57, txtGrid_58, txtGrid_60, txtGrid_61, txtGrid_62, txtGrid_63, txtGrid_64, txtGrid_65, txtGrid_66, txtGrid_67, txtGrid_68, txtGrid_70, txtGrid_71, txtGrid_72, txtGrid_73, txtGrid_74, txtGrid_75, txtGrid_76, txtGrid_77, txtGrid_78, txtGrid_80, txtGrid_81, txtGrid_82, txtGrid_83, txtGrid_84, txtGrid_85, txtGrid_86, txtGrid_87, txtGrid_88}

        txtVersion.Text = "Version: " & GetAppVersion()

        txtCopyright.Text = "© " & DateTime.Now.ToString("yyyy") & " APRA.tech"

        _info.Visibility = Visibility.Collapsed
        btnSolve.IsEnabled = False
        btnHint.IsEnabled = False
        SetNumbers(False)
    End Sub

    Private Sub SetArray(ByVal x As Integer, ByVal y As Integer, ByVal z As Integer)
        ArrayMulti(x, y) = z
    End Sub

    Private Sub SetNumbers(ByVal bool As Boolean)
        btnNo1.IsEnabled = bool
        btnNo2.IsEnabled = bool
        btnNo3.IsEnabled = bool
        btnNo4.IsEnabled = bool
        btnNo5.IsEnabled = bool
        btnNo6.IsEnabled = bool
        btnNo7.IsEnabled = bool
        btnNo8.IsEnabled = bool
        btnNo9.IsEnabled = bool
        btnErase.IsEnabled = bool
    End Sub

    Private Sub ShowInfo(ByVal x As Integer)
        ScrollViewInfo.ChangeView(Nothing, 0, Nothing, True)
        Select Case x
            Case 0
                txtInfoText.Text = ABoutInfo
                txtInfoTitle.Text = "About"
            Case 1
                txtInfoText.Text = LibraryInfo
                txtInfoTitle.Text = "3rd Party Libraries"
        End Select
        _info.Visibility = Visibility.Visible
    End Sub

    Private Sub btnLevel1Menu_Click(sender As Object, e As RoutedEventArgs) Handles btnLevel1Menu.Click
        CloseMenu()
        GenerateBoard("easy")
    End Sub

    Private Sub btnLevel2Menu_Click(sender As Object, e As RoutedEventArgs) Handles btnLevel2Menu.Click
        CloseMenu()
        GenerateBoard("medium")
    End Sub

    Private Sub btnLevel3Menu_Click(sender As Object, e As RoutedEventArgs) Handles btnLevel3Menu.Click
        CloseMenu()
        GenerateBoard("hard")
    End Sub

    Private Sub btnLevel4Menu_Click(sender As Object, e As RoutedEventArgs) Handles btnLevel4Menu.Click
        CloseMenu()
        GenerateBoard("random")
    End Sub

    Private Sub GeneratedBoardSolution()
        Dim solved As Boolean = SudokuSolver.SudokuSolver.Solve(ArraySolution)
        Dim Lines(8) As String
        Dim Cntr As Integer = 0
        If solved Then
            For i As Integer = 0 To 9 - 1

                For j As Integer = 0 To 9 - 1
                    Lines(i) = Lines(i) & ArraySolution(i, j) & " "
                    ArrayGrid(Cntr) = ArraySolution(i, j)
                    LabelArray(Cntr).Text = ArrayGrid(Cntr)
                    Cntr += 1
                Next
            Next
        End If
    End Sub

    Private Async Sub GenerateBoard(ByVal x As String)
        ProgRing.IsActive = True
        Dim http = New HttpClient()
        Dim url = String.Format("https://sugoku.herokuapp.com/board?difficulty=" & x)
        Dim response = Await http.GetAsync(url)
        Dim result = Await response.Content.ReadAsStringAsync()
        Dim TestObject As GeneratedRandomBoard = Global.Newtonsoft.Json.JsonConvert.DeserializeObject(Of GeneratedRandomBoard)(result.ToString)
        Dim Cntr As Integer = 0
        For a = 0 To 8
            For b = 0 To 8
                ArrayMulti(a, b) = TestObject.board.ElementAt(a).ElementAt(b)
                ArraySolution(a, b) = ArrayMulti(a, b)
                MainButtonArray(Cntr).Content = ArrayMulti(a, b)
                Cntr += 1
            Next
        Next

        GeneratedBoardSolution()

        For a = 0 To 8
            For b = 0 To 8
                If ArrayMulti(a, b) <> 0 Then
                    NumberCount += 1
                End If
            Next
        Next

        If NumberCount > 17 Then
            btnSolve.IsEnabled = True
            btnHint.IsEnabled = True
        Else
            btnSolve.IsEnabled = False
            btnHint.IsEnabled = False
        End If

        ProgRing.IsActive = False

    End Sub

    Private Sub ShowBoard()
        Dim cntr As Integer = 0
        For a = 0 To 8
            For b = 0 To 8
                MainButtonArray(cntr).Content = ArrayMulti(a, b)
                cntr += 1
            Next
        Next
    End Sub

    Private Sub CloseMenu()
        IsMenuOpen = False
        SplitMenu.IsPaneOpen = Not SplitMenu.IsPaneOpen
    End Sub

    Private Sub btnMenu_Click(sender As Object, e As RoutedEventArgs) Handles btnMenu.Click
        SplitMenu.IsPaneOpen = Not SplitMenu.IsPaneOpen
    End Sub

End Class