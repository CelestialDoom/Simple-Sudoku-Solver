[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)  [![GPL Licence](https://badges.frapsoft.com/os/gpl/gpl.png?v=103)](https://opensource.org/licenses/GPL-3.0/)  [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/)  [![HitCount](http://hits.dwyl.com/CelestialDoom/Simple-Sudoku-Solver.svg)](http://hits.dwyl.com/CelestialDoom/Simple-Sudoku-Solver)  [![GitHub contributors](https://img.shields.io/github/contributors/CelestialDoom/Simple-Sudoku-Solver.svg)](https://github.com/CelestialDoom/Simple-Sudoku-Solver/graphs/contributors)  [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/CelestialDoom/Simple-Sudoku-Solver/issues)

![Simple-Sudoku-Solver](https://i.postimg.cc/RFVqj0HX/Sudoku-Black-Icon.png)
# Simple Sudoku Solver

Simple Sudoku Solver is a UWP app for Windows 10 app that will allow the user to enter, and solve sudoku puzzles.
Designed to be used on  :iphone: :computer:

If there are any suggestions, contact me :email:

Small memory footprint, open source, and forever free.
***
This app does not send ANY data to remote servers, although, the app does use an API service to generate the sudoku grid.

This API service is located at [# suGOku](https://github.com/berto/sugoku)
***
__Whats in the future for this app?__

~~At some point, I'd like to have the app able to generate sudoku puzzles.
I've got a couple of ideas, but, they'll need more work doing to it.
Also, I'd have to change a few things here, on this README.md file.~~

Well, I've gotten the board generator out of the way.

So, what's happeing?

Adding a Load / Save function.
This will save up to 10 games in local storage(?), and allow you to then (surprise, surprise) load them back in.
This *might* take a bit of time to do.

For any other future features, if you have any, contact me.

***

## Getting Started

This UWP app does not need any special installations. If run directly from Visual Stidio 2017 (Community/Professional/Enterprise) it will install directly on the device.

### Prerequisites

```
Visual Studio 2017 (Any version).

For the mobile version, a physical smartphone would be useful, otherwise the emulator will suffice.
```

### Installing

Start Visual Studio, open project, begin.

Simple!

## Built With

 [Visual Studio 2017](http://www.visualstudio.com/vs/)
 
The IDE used (Yeah, I wrote it in VB.net, so what?)

## Authors

* **Celestial Doom** - *Initial work* - [CelestialDoom](https://github.com/CelestialDoom)

See also the list of [contributors](https://github.com/CelestialDoom/All-Social/contributors) who participated in this project (When one gets added).

## Donate

I do all this work for free, if you'd like to [donate to me](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=C8GGT76WWEKPY&lc=GB&item_name=APRA%2etech&currency_code=GBP&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted) (a cup of coffee :coffee:, or a pint of hard cider :beer:), I'd be mighty appreciative.

Any donators will be mentioned in the about section of the app :+1:



## License

[![GPL Licence](https://badges.frapsoft.com/os/gpl/gpl.png?v=103)](https://opensource.org/licenses/GPL-3.0/)

This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3 - see the [LICENSE](https://github.com/CelestialDoom/Simple-Sudoku-Solver/blob/master/LICENCE) file for details

## Acknowledgments

* CodeMaid [CodeMaid Visual Studio Extension] (http://www.codemaid.net/)
* Celestial Doom [Custom Made Icon](https://i.postimg.cc/RFVqj0HX/Sudoku-Black-Icon.png)
* [Telerik Code Converter](http://converter.telerik.com/) - Yeah, I've had to convert C# to VB.net a few times
* [JSON Utils](http://jsonutils.com/) - This is **REALLY** useful if you need to parse JSON
* Hat tip to anyone who's code was used
* Inspiration
* etc
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3MjUwOTAzOTddfQ==
-->